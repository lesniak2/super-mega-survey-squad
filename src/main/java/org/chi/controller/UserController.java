package org.chi.controller;

import org.chi.dao.UserDao;
import org.chi.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class UserController {

    @Autowired
    private UserDao userDao;

    @RequestMapping("/create")
    @ResponseBody
    public String create(String name, String phoneNumber, String zipCode) {
        String userId = "";
        try {
            User user = new User(name, phoneNumber, zipCode);
            userDao.store(user);
            userId = String.valueOf(user.getId());
        }
        catch (Exception e) {
            return "Error creating user " + name + ": " + e.toString();
        }
        return "User " + name + ": successfully created with id = " + userId;
    }

    @RequestMapping("/delete")
    @ResponseBody
    public String delete(Long id) {
        try {
            User user = new User(id);
            userDao.delete(user);
        }
        catch (Exception e) {
            return "Error deleting the user " + id + ": " + e.toString();
        }
        return "User " + id + " successfully deleted";
    }

    @RequestMapping("/get-by-phone")
    @ResponseBody
    public String getByPhone(String phoneNumber) {
        String userId = "";
        try {
            User user = userDao.getByPhone(phoneNumber);
            userId = String.valueOf(user.getId());
        }
        catch (Exception e) {
            return "User not found";
        }
        return "The user id is: " + userId;
    }

    @RequestMapping("/update")
    @ResponseBody
    public String updateUser(Long id, String name, String zipCode, String phoneNumber) {
        try {
            User user = userDao.getById(id);
            user.setName(name);
            user.setName(zipCode);
            user.setName(phoneNumber);
            userDao.store(user);
        }
        catch (Exception e) {
            return "Error updating the user " + name + ": " + e.toString();
        }
        return "User " + name + " successfully updated";
    }
}
