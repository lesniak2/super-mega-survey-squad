package org.chi.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RootController {

    @RequestMapping("/")
    @ResponseBody
    public String index() {
        return "Created with pride by the Super Mega Survey Squad during" +
                "<a href='https://www.chicagosfoodbank.org'>GCFD's Hackathon to End Hunger 2017</a>";
    }

}