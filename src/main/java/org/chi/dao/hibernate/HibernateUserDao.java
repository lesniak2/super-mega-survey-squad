package org.chi.dao.hibernate;

import org.chi.dao.UserDao;
import org.chi.entity.User;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

@Transactional
@Repository
public class HibernateUserDao extends AbstractHibernateDao<User> implements UserDao {

    @SuppressWarnings("unchecked")
    protected HibernateUserDao(Class<User> entityClass, SessionFactory sessionFactory) {
        super(entityClass, sessionFactory);
    }

    @Override
    public void store(User user) {
        Assert.notNull(user, "user cannot be null");
        currentSession().saveOrUpdate(user);
        currentSession().flush();
    }

    @Override
    public void delete(User user) {
        Assert.notNull(user, "user cannot be null");
        currentSession().delete(user);
        currentSession().flush();
    }

    @Override
    public User getById(Long userId) {
        Assert.notNull(userId, "argument userId cannot be null");
        return get(userId);
    }

    @Override
    public User getByPhone(String phoneNumber) {
        Assert.notNull(phoneNumber, "argument phoneNumber cannot be null");
        Criteria criteria = criteria();
        criteria.add(Restrictions.eq("phoneNumber", phoneNumber));
        return uniqueResult(criteria);
    }

    @Override
    public List<User> getByName(String name) {
        Assert.notNull(name, "argument name cannot be null");
        Criteria criteria = criteria();
        criteria.add(Restrictions.eq("name", name));
        return list(criteria);
    }
}
