package org.chi.dao.hibernate;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.io.Serializable;
import java.util.List;

public class AbstractHibernateDao<E> {
    private final Class<E> entityClass;
    private final SessionFactory sessionFactory;

    protected AbstractHibernateDao(Class<E> entityClass, SessionFactory sessionFactory) {
        if(entityClass == null) {
            throw new IllegalArgumentException("argument entityClass must not be null");
        }
        else if(sessionFactory == null) {
            throw new IllegalArgumentException("argument sessionFactory must not be null");
        }
        else {
            this.entityClass = entityClass;
            this.sessionFactory = sessionFactory;
        }
    }

    public Class<E> getEntityClass() { return this.entityClass; }

    protected Criteria criteria() { return this.currentSession().createCriteria(this.entityClass); }

    protected Query query(String hql) { return this.currentSession().createQuery(hql); }

    protected Session currentSession() { return this.sessionFactory.getCurrentSession(); }

    protected SessionFactory getSessionFactory() { return this.sessionFactory; }

    protected Session openSession() { return this.sessionFactory.openSession(); }

    protected List<E> all() { return this.list(this.criteria()); }

    @SuppressWarnings("unchecked")
    protected List<E> list(Criteria criteria) { return criteria.list(); }

    @SuppressWarnings("unchecked")
    protected List<E> list(Query query) { return (List<E>) query.list(); }

    @SuppressWarnings("unchecked")
    protected E uniqueResult(Criteria criteria) { return (E) criteria.uniqueResult(); }

    @SuppressWarnings("unchecked")
    protected E uniqueResult(Query query) { return (E) query.uniqueResult(); }

    protected E get(Serializable id) { return this.currentSession().get(this.entityClass, id); }

    protected E load(Serializable id) { return this.currentSession().load(this.entityClass, id); }
}
