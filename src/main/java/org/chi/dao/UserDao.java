package org.chi.dao;

import org.chi.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface UserDao {

    void store(User user);

    void delete(User user);

    User getById(Long userId);

    User getByPhone(String phoneNumber);

    List<User> getByName(String name);
}
